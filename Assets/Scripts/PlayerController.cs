using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator m_animator;
    public Transform circle;
    // Start is called before the first frame update
    void Start()
    {
        m_animator = GetComponent<Animator>();

        m_animator.SetFloat("MoveSpeed", 1f);
        m_animator.SetBool("Grounded", true);
    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = Quaternion.Euler(0, circle.rotation.y * 3f, 0);
        //RotateHorizontal();
    }
}
