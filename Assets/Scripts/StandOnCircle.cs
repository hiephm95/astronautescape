using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandOnCircle : MonoBehaviour
{
    public float gravity = -12;
    public Transform plane;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 gravityUp = (transform.position - plane.position).normalized;
        Vector3 localUp = transform.up;

        transform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

        Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * transform.rotation;
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 50 * Time.deltaTime);
    }
}
